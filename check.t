# $MirOS: src/usr.bin/rs/check.t,v 1.7 2020/03/13 00:41:48 tg Exp $

name: debian-650029-e
description:
	Check basic output
stdin:
	1
	a wide column
	2
	3
	4
	a
	another wide column
	b
	c
	d
arguments: !-e!0!5!
expected-stdout:
	1                    a wide column        2                    3                    4
	a                    another wide column  b                    c                    d
---
name: debian-650029-ze
description:
	Check output with -z
stdin:
	1
	a wide column
	2
	3
	4
	a
	another wide column
	b
	c
	d
arguments: !-ze!0!5!
expected-stdout:
	1  a wide column        2  3  4
	a  another wide column  b  c  d
---
name: debian-668306
description:
	Check it doesn’t immediately segfault
stdin:
	1
	2
	3
	4
	5
	6
	7
	8
	9
arguments: !3!3!
expected-stdout:
	1  2  3
	4  5  6
	7  8  9
---
name: debian-706717-r1.10
description:
	Handle empty fields
stdin:
	0	0
		0
arguments: !-c!0!2!
expected-stdout:
	0  0
	   0
---
name: debian-706717-r1.11-1
description:
	Handle empty last fields
stdin:
	0	0
	0
arguments: !-c!0!2!
expected-stdout:
	0  0
	0  
---
name: debian-706717-r1.11-2
description:
	Handle empty last fields
stdin:
	0	0
	0	
arguments: !-c!0!2!
expected-stdout:
	0  0
	0  
---
name: empty-last-line-1
description:
	Basis for the next test
stdin:
	a b c d
arguments: !0!2!
expected-stdout:
	a  b
	c  d
---
name: empty-last-line-2
description:
	There should be no empty line just due to a trailing space
stdin:
	a b c d 
arguments: !0!2!
expected-stdout:
	a  b
	c  d
---
name: unicode-1
description:
	Fun with UTF-8
stdin:
	mäh miau a Kuh 猫 b ❣❧ マヌル c
env-setup: !LC_ALL=C.UTF-8!
arguments: !0!3!
expected-stdout:
	mäh     miau    a
	Kuh     猫      b
	❣❧      マヌル  c
---
name: unicode-2
description:
	Fun with UTF-8
stdin:
	mäh miau a Kuh 猫 b ❣❧ マヌル c
env-setup: !LC_ALL=C.UTF-8!
arguments: !-z!0!3!
expected-stdout:
	mäh  miau    a
	Kuh  猫      b
	❣❧   マヌル  c
---
name: unicode-3-utf8
description:
	Invalids
category: !os:mirbsd
stdin:
	� b c ��
env-setup: !LC_ALL=C.UTF-8!
arguments: !-z!0!2!
expected-stdout:
	?  b
	c  ??
---
name: unicode-3-optu8
description:
	Invalids
category: os:mirbsd
stdin:
	� b c ��
env-setup: !LC_ALL=C.UTF-8!
arguments: !-z!0!2!
expected-stdout:
	�  b
	c  ��
---
name: unicode-4
description:
	ASCII
category: !os:mirbsd
stdin:
	ä b c d
env-setup: !LC_ALL=C!
arguments: !-z!0!2!
expected-stdout:
	ä  b
	c   d
---
