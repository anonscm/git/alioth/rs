# $MirOS: src/usr.bin/rs/Makefile,v 1.3 2020/03/11 21:30:10 tg Exp $
# $OpenBSD: Makefile,v 1.3 2015/12/03 12:23:15 schwarze Exp $

PROG=		rs
SRCS=		rs.c utf8.c

MKSHDIR?=	${BSDSRCDIR}/bin/mksh
SRCDIR?=	${.CURDIR}

regress: ${PROG} check.t
	perl ${MKSHDIR:Q}/check.pl -s ${SRCDIR:Q}/check.t -v -p ./${PROG:Q}

.include <bsd.prog.mk>
